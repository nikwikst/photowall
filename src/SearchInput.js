import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { Columns, Column, Field, Label, Control, Input } from 'bloomer';

@inject('store')
class SearchInput extends Component {
  handleChange(event) {
    this.props.store.searchText = event.target.value;
  }

  render() {
    return (
      <Columns isCentered>
        <Column
          isOffset={{ mobile: 2 }}
          isSize={{ mobile: 8, tablet: 6, desktop: 3 }}
        >
          <Field>
            <Label>Get photos with tag:</Label>
            <Control>
              <Input
                type="text"
                placeholder="Tag Input"
                defaultValue={this.props.store.searchText}
                onChange={e => this.handleChange(e)}
              />
            </Control>
          </Field>
        </Column>
      </Columns>
    );
  }
}

export default SearchInput;

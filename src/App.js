import React, { Component } from 'react';
import { Container } from 'bloomer';
import { Provider, observer } from 'mobx-react';

import SearchInput from './SearchInput';
import Images from './Images';

import imageStore from './ImageStore';

@observer
class App extends Component {
  render() {
    return (
      <Provider store={imageStore}>
        <Container isFluid>
          <SearchInput />
          <Images />
        </Container>
      </Provider>
    );
  }
}

export default App;

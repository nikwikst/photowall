import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Image } from 'bloomer';

@inject('store')
@observer
class ImageItem extends Component {
  constructor(props) {
    super(props);
    this.key = props.index;
    this.state = { show: false, index: props.index };

    this.hideStyle = {
      opacity: 0,
      transition: 'all 1s ease'
    };

    this.showStyle = {
      opacity: 1,
      transition: 'all 1s ease'
    };
  }

  componentWillUnmount() {
    this.setState({ show: false });
    clearTimeout(this.idTimeout);
  }

  onLoad(e) {
    if (this.state.show) return;

    // image is loaded => animate opacity to make it visible
    if (e.target.naturalWidth > e.target.naturalHeight) {
      this.isLandscape = true;
    }

    this.setState({ show: true });

    this.idTimeout = setTimeout(() => {
      this.setState({ show: false });
    }, 1000 + parseInt(Math.random() * 13000, 10));
  }

  transitionEnd() {
    // render new image when image has animated out
    if (!this.state.show && this.props.store.isNewImagesLoaded) {
      this.setState({ index: this.props.store.nextImageIndex });
    }
  }

  render() {
    let renderStyle = this.state.show ? this.showStyle : this.hideStyle;

    return (
      this.props.store.isNewImagesLoaded && (
        <div
          key={this.key}
          style={renderStyle}
          onTransitionEnd={event => this.transitionEnd(event)}
        >
          <Image
            isRatio="4:3"
            className={this.isLandscape ? 'landscape' : 'portrait'}
            src={this.props.store.items[this.state.index].media.m}
            onLoad={event => this.onLoad(event)}
          />
        </div>
      )
    );
  }
}

export default ImageItem;

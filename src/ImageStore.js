import { observable, autorun } from 'mobx';

class ImageStore {
  @observable isNewImagesLoaded;
  @observable searchText = 'alp';

  modified = '';
  jsonResponse = [];
  items = [];

  nrImagesToShow = 12;
  reloadInterval = 20;
  // lastImageIndex;

  urlFlickr = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=handleTheResponse';

  constructor() {
    this.getJson();

    // is run when searchtext is changed
    autorun(() => {
      this.getJson();
    });
  }

  // workaround to make cross-site-loading work in localhost
  getJson() {
    window['handleTheResponse'] = this.handleTheResponse.bind(this);

    var script = document.createElement('script');
    script.src = this.urlFlickr + '&tags=' + this.searchText;
    document.head.appendChild(script);

    // keep loading
    clearInterval(this.idInterval);
    this.idInterval = setInterval(() => {
      this.getJson();
    }, 1000 * this.reloadInterval);
  }

  handleTheResponse(data) {
    this.jsonResponse = data;

    // any new images?
    if (this.modified !== this.jsonResponse.modified) {
      this.isNewImagesLoaded = false;
      this.updateVariables();
      console.log('json updated');
    } else {
      console.log('json not modified');
    }
  }

  updateVariables() {
    this.items = this.jsonResponse.items;
    this.modified = this.jsonResponse.modified;
    this.isNewImagesLoaded = true;
    this.lastImageIndex = this.nrImagesToShow;
  }

  updateImageIndex() {
    // loop loaded images when all are shown
    this.lastImageIndex =
      this.lastImageIndex + 1 >= this.items.length
        ? 0
        : this.lastImageIndex + 1;
  }

  get nextImageIndex() {
    this.updateImageIndex();
    return this.lastImageIndex;
  }
}

const imageStore = new ImageStore();
export default imageStore;

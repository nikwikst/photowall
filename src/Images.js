import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Columns, Column } from 'bloomer';
import ImageItem from './ImageItem';

@inject('store')
@observer
class Images extends Component {
  render() {
    let imageElement = null;

    if (!this.props.store.isNewImagesLoaded) {
      imageElement = <div />;
    } else {
      imageElement = this.props.store.items
        .filter((i, index) => index < this.props.store.nrImagesToShow)
        .map((image, index) => {
          return (
            <Column
              key={index}
              isSize={{ mobile: '1/2', tablet: '1/3', desktop: '1/4' }}
            >
              <ImageItem key={index} index={index} />
            </Column>
          );
        });
    }

    return (
      <Columns isMobile isMultiline isGapless>
        {imageElement}
      </Columns>
    );
  }
}

export default Images;
